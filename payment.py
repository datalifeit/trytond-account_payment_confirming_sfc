# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import os
import phonenumbers
from phonenumbers import NumberParseException
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Journal', 'Group', 'Payment']


class Journal(metaclass=PoolMeta):
    __name__ = 'account.payment.journal'

    @classmethod
    def __setup__(cls):
        super(Journal, cls).__setup__()
        cls.confirming_payable_flavor.selection.append(
            ('sfc53', 'SFC v5.3'))


class Group(metaclass=PoolMeta):
    __name__ = 'account.payment.group'

    @classmethod
    def __setup__(cls):
        super(Group, cls).__setup__()
        _state = (
            (Eval('process_method') == 'confirming') &
            (Eval('_parent_journal', {}).get(
                'confirming_payable_flavor') == 'sfc53'))
        if cls.planned_date.states.get('required'):
            cls.planned_date.states['required'] |= _state
        else:
            cls.planned_date.states['required'] = _state
        cls.planned_date.depends.append('process_method')
        cls._error_messages.update({
            'sfc53_header_zoneb':
                'Must define B1 or B2+B3 or B4+B5 in Journal "%s" for '
                'confirming header record.',
            'sfc53_detail_zonec':
                'Must define C1 in Journal "%s" for confirming header record.',

        })

    @classmethod
    def _get_confirming_template_paths(cls):
        values = super(Group, cls)._get_confirming_template_paths()
        values.update({
            'sfc53.txt': os.path.join(os.path.dirname(__file__),
                'template')
            })
        return values

    def _check_confirming_fields(self):
        if self.journal.confirming_payable_flavor != 'sfc53':
            super(Group, self)._check_confirming_fields()
            return
        attrs = self.get_confirming_fields() or {}
        if not attrs.get('codigo_operacion') and (
                    not attrs.get('codigo_cliente') or
                    not attrs.get('num_operacion')
                ) and (
                    not self.company.party.tax_identifier or
                    not attrs.get('sufijo')):
            self.raise_user_error('sfc53_header_zoneb',
                self.journal.rec_name)
        if not attrs.get('forma_pago'):
            self.raise_user_error('sfc53_detail_zonec',
                self.journal.rec_name)


class Payment(metaclass=PoolMeta):
    __name__ = 'account.payment'

    @classmethod
    def __setup__(cls):
        super(Payment, cls).__setup__()
        cls._error_messages.update({
            'sfc53_detail_zoneb':
                'Must define B2 and B4 in Party "%s" for confirming '
                'detail record.',
            'sfc53_detail_zoneb_surname':
                'Must define B5, B6 and B7 in Party "%s" for confirming '
                'detail record.',
            'sfc53_detail_zoneb_address':
                'Must define B9 in Party address "%s" for confirming '
                'detail record.',
            'sfc53_phone_format': 'Invalid format for phone or fax number '
                '"%s" in Party "%s".'
        })

    def _check_confirming_fields(self):
        if self.journal.confirming_payable_flavor != 'sfc53':
            super(Payment, self)._check_confirming_fields()
            return
        attrs = self.get_confirming_fields() or {}
        if not attrs.get('tipo_identificador') or \
                not attrs.get('tipo_persona'):
            self.raise_user_error('sfc53_detail_zoneb',
                self.party.rec_name)
        if attrs['tipo_persona'] == 'F':
            for item in ('apellido1', 'apellido2', 'nombre'):
                if not attrs.get(item):
                    self.raise_user_error('sfc53_detail_zoneb_surname',
                        self.party.rec_name)
        address = self._get_confirming_address()
        if not attrs.get('tipo_via'):
            self.raise_user_error('sfc53_detail_zoneb_address',
                address.rec_name)

    def _get_confirming_contact_mechanisms(self):
        values = super(Payment, self)._get_confirming_contact_mechanisms()
        if self.journal.confirming_payable_flavor != 'sfc53':
            return values
        for mech in ('phone', 'fax'):
            value = values[mech]
            if not value:
                continue
            try:
                phonenumbers.parse(value)
            except NumberParseException:
                self.raise_user_error('sfc53_phone_format', (
                    value, self.party.rec_name))
        return values

    def _get_confirming_bank_account(self):
        res = super(Payment, self)._get_confirming_bank_account()
        if (self.line.origin and
                self.line.origin.__name__ == 'account.invoice'):
            return self.line.origin.bank_account or res
        return res
